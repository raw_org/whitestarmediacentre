package MediaPlayer;

import javafx.util.Duration;

import com.sun.media.jfxmedia.MediaException;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class MP3Player implements MediaTypePlayer {
	
	private MediaPlayer playingControls;
	private String mediaSource;
	private Media jfxMedia;
	private Duration forwardSeek = new Duration(20.0);
	private Duration backwardSeek = new Duration(-10.0);
	
	public void setMedia(String mediaSource) {
		playingControls.stop();	
		this.mediaSource = mediaSource;
		jfxMedia = new Media(mediaSource);
	}

	@Override
	public void play() {
		playingControls.play();
	}

	@Override
	public void pause() {
		playingControls.pause();

	}

	@Override
	public void forward() {
		playingControls.seek(forwardSeek);
	}

	@Override
	public void rewind() {
		playingControls.seek(backwardSeek);

	}

	@Override
	public void setVolume(int level) {
		// ensure the level is between 0 and 1
        if(level > 1 || level <0){
            //do nothing
            }
            else{
                playingControls.setVolume(level);
                }

	}

	@Override
	public void stop() {
		playingControls.stop();

	}

	@Override
	public void goToPositionInTrack(Duration pointInTime) {
		 if(pointInTime.lessThan(playingControls.getMedia().getDuration())){
             playingControls.setStartTime(pointInTime);
             playingControls.play();
            }

	}

}
