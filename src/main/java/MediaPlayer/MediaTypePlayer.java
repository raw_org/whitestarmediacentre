package MediaPlayer;

import javafx.util.Duration;

public interface MediaTypePlayer {

public void setMedia(String mediaSource);

public void play();

public void pause();

public void forward();

public void rewind();

public void setVolume(int level);

public void stop();

public void goToPositionInTrack(Duration pointInTime);
	
}
