package MediaPlayer;

import java.io.File;

import javafx.util.Duration;

public class AutoTypePlayer implements MediaTypePlayer {
	
	private MediaTypePlayer currentPlayer;

	@Override
	public void setMedia(String mediaSource) {
		currentPlayer.stop();
		setCurrentPlayer(mediaSource);

	}

	private void setCurrentPlayer(String mediaSource) {
        //establish if the file exists
        File mediaFile 	= new File(mediaSource);
        if(mediaFile.exists()){
         //establish the file type
         String type = mediaFile.getName();//get the file name
         type = type.substring(type.lastIndexOf("."));//get the extenstion
         if (type.equalsIgnoreCase(".mp3"))
         {
             currentPlayer = new MP3Player();
             currentPlayer.setMedia(mediaSource);
             }
             else{
                 if(type.equalsIgnoreCase(".ogg")){
                     currentPlayer = new OggPlayer();
                     currentPlayer.setMedia(mediaSource);
                     }
                 }
        
            System.out.println("file does not exist");
            }
            else {
                
                System.out.println("Unable to play this file source " + mediaSource);

                }
        
		
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void forward() {
		// TODO Auto-generated method stub

	}

	@Override
	public void rewind() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setVolume(int level) {
		// TODO Auto-generated method stub

	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}

	@Override
	public void goToPositionInTrack(Duration pointInTime) {
		// TODO Auto-generated method stub

	}

}
