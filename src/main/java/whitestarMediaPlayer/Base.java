package whitestarMediaPlayer;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class Base extends Group{
	
	private int height;

	public Base() {
		super();
			this.getChildren().addAll(
					drawBaseCircle(),
					drawCross()
					);
		
	}

	private Node drawCross() {		
		Group node = new Group();
				node.getChildren().addAll(
				drawVerticleLine(),
				drawHorizontalLine()
				);
				return node;
	}

	private Node drawHorizontalLine() {
		return new Line(
				height/2,//Start X
				0, //Start Y
				height/2,//End X
				height //End Y 
				);
	}

	private Node drawVerticleLine() {	
		return new Line(
				0,//start X
				height/2,//start y
				height,//end x
				height/2//end y
				);
				
	}

	private Node drawBaseCircle() {	
		return configureBaseCircle(new Circle(height/2, height/2, height));
	}

	private Node configureBaseCircle(Circle circle) {
		circle.setCenterX(height/2);
		circle.setCenterY(height/2);
		return circle;
	}
	
	
	
}
